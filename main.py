import argparse

import json_handler
import discord_handler

def set_up_arguments():
    presetArguments = argparse.ArgumentParser()
    presetArguments.add_argument("-n", "--torrent-name", required=True, type=str)
    presetArguments.add_argument("-c", "--categories", required=True, type=str)
    presetArguments.add_argument("-th", "--hash", required=True, type=str)
    presetArguments.add_argument("-l", "--labels", required=True, type=str)
    return presetArguments

def main():
    arguments = vars(set_up_arguments().parse_args())
    webhookUrl = json_handler.read_json("config.json")['WebhookUrl']
    parsed_labels = arguments['labels'].split(',')
    if "Blacklisted" not in parsed_labels:
        template = json_handler.read_json("template.json")
        discord_handler.post_to_webHook(template, arguments['torrent_name'],
            arguments['categories'], arguments['hash'], webhookUrl)

if __name__ == '__main__':
    main()
