import json

def read_json(jsonPath):
    with open(jsonPath, "r") as jsonFile:
        data = json.load(jsonFile)
    return data
